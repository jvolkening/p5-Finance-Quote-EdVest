package Finance::Quote::EdVest 1.000;

use strict;
use warnings;
use 5.012;

use XML::LibXML;
use HTTP::Tiny;
use Time::Piece;

# URLs of where to obtain information
my $ev_url = 'https://www.edvest.com/investment/price-performance';

my %ev_fund_rows = (
    EVEY40 => "2040/2041 Enrollment",
    EVEY38 => "2038/2039 Enrollment",
    EVEY36 => "2036/2037 Enrollment",
    EVEY34 => "2034/2035 Enrollment",
    EVEY33 => "2032/2033 Enrollment",
    EVEY30 => "2030/2031 Enrollment",
    EVEY28 => "2028/2029 Enrollment",
    EVEY26 => "2026/2027 Enrollment",
    EVEY24 => "2024/2025 Enrollment",
    EVIS   => "In School",
    EVPPI  => "Principal Plus Interest",
    EVIBA  => "Index-Based Aggressive",
    EVIBM  => "Index-Based Moderate",
    EVIBC  => "Index-Based Conservative",
    EVABA  => "Active-Based Aggressive",
    EVABM  => "Active-Based Moderate",
    EVABC  => "Active-Based Conservative",
    EVI    => "International Equity Index",
    EVB    => "Balanced",
    EVSCI  => "Small-Cap Index",
    EVUSA  => "U.S. Equity Active",
    EVLCI  => "Large-Cap Stock Index",
    EVSOC  => "Social Choice",
    EVBOND => "Bond Index",
);

sub methods { return (edvest => \&edvest) }

sub err_hash {

    my ($wantarray, $msg, @symbols) = @_;

    my %err;
    $err{$_, 'errormsg'} = $msg for (@symbols);
    $err{$_, 'success'}  = 0    for (@symbols);

    return $wantarray ? %err : \%err;

}

 
sub edvest {

    my ($quoter, @symbols) = @_;
    my %info;

	my $ua = HTTP::Tiny->new;
	my $resp = $ua->get($ev_url);
    return err_hash(wantarray, $resp->{reason}, @symbols)
        if (! $resp->{success});
    
    my $xml = XML::LibXML->load_html(
        string          => $resp->{content},
        recover         => 1,
        suppress_errors => 1,
    );

    my %navs;

    TBL:
    for my $tbl ($xml->findnodes('//div[@class="price-performance-table-container"]/table[@class="responsive-table"]')) {

        my $date;
        HDR:
        for my $row ($tbl->findnodes('./thead/tr')) {
            for my $cell ($row->findnodes('./th/text()')) {
                if ($cell =~ /^Daily as of (\w+ \d{1,2}, \d{4})$/) {
                    $date = Time::Piece->strptime($1, "%B %d, %Y")
                        or return err_hash(wantarray, 'Date parse error: $@', @symbols);
                    last HDR;
                }
            }
        }
        if (! defined $date) {
            return err_hash(wantarray, 'Failed to find date element', @symbols);
        }

        ROW:
        for my $row ($tbl->findnodes('./tr')) {

            # extract name
            my @cells = $row->findnodes('./th/a[@class="fundname"]/text()');
            next ROW
                if (scalar @cells != 1);
            my $name = $cells[0];
            $name =~ s/\s+Portfolio$//;
            
            # extract price
            @cells = $row->findnodes('./td/span[starts-with(text(), "Unit Value as of")]/../text()');
            next ROW
                if (scalar @cells != 1);
            my $nav = $cells[0];
            if ($nav =~ /^\$([\d\.]+)$/) {
                $nav = $1;
            }
            else {
                return err_hash(wantarray, 'NAV parse error: $@', @symbols);
            }

            $navs{$name}->{price} = $nav;
            $navs{$name}->{date}  = $date;
        }
    }

	for my $symb (@symbols) {

	    my $name = $ev_fund_rows{ uc $symb };

        if (! defined $name) {
            $info{$symb, 'success'} = 0;
            $info{$symb, 'errormsg'} = 'Unknown fund';
	    }
        elsif (! defined $navs{$name}) {
            $info{$symb, 'success'} = 0;
            $info{$symb, 'errormsg'} = 'Error parsing data source';
	    }
	    else {
            $info{$symb, 'success' } = 1;
            $info{$symb, 'method'  } = 'edvest';
            $info{$symb, 'currency'} = 'USD';
            $info{$symb, 'source'  } = $ev_url;
            $info{$symb, 'symbol'  } = $symb;
            $info{$symb, 'name'    } = $name;
            $info{$symb, 'nav'     } = $navs{$name}->{price};
            $info{$symb, 'last'    } = $navs{$name}->{price};
            $info{$symb, 'price'   } = $navs{$name}->{price};
            $info{$symb, 'date'    } = $navs{$name}->{date}->mdy('/');
            $info{$symb, 'isodate' } = $navs{$name}->{date}->ymd;
	    }
	}
	return wantarray ? %info : \%info;

} 
{
    my @labels = qw/
        success method currency source symbol
        name nav last price date isodate/;

    sub labels {
        return ( edvest => \@labels,
        );
    }
}

1; 

=head1 NAME

Finance::Quote::EdVest - Obtain fund prices for EdVest plans

=head1 SYNOPSIS

    use Finance::Quote;

    my $q    = Finance::Quote->new;
    my %info = Finance::Quote->fetch('edvest' => 'EVBOND'); 

=head1 DESCRIPTION

This module fetches fund information for the EdVest funds

    https://www.edvest.com/

using its fund prices page

    http://www.edvest.com/research/daily.shtml

The quote symbols are

    * EVEY40   2040/2041 Enrollment

    * EVEY38   2038/2039 Enrollment

    * EVEY36   2036/2037 Enrollment

    * EVEY34   2034/2035 Enrollment

    * EVEY33   2032/2033 Enrollment

    * EVEY30   2030/2031 Enrollment

    * EVEY28   2028/2029 Enrollment

    * EVEY26   2026/2027 Enrollment

    * EVEY24   2024/2025 Enrollment

	* EVPPI    Principal Plus Interest Portfolio

	* EVIBA    Index-Based Aggressive

	* EVIBM    Index-Based Moderate

	* EVIBC    Index-Based Conservative

	* EVABA    Active-Based Aggressive

	* EVABM    Active-Based Moderate

	* EVABC    Active-Based Conservative

	* EVI      International Equity Index

	* EVB      Balanced

	* EVSCI    Small-Cap Index Portfolio

	* EVUSA    U.S. Equity Active Portfolio

	* EVLCI    Large Cap Stock Index Portfolio

	* EVSOC    Social Choice Portfolio

	* EVBOND   Bond Index Portfolio
    

=head1 SEE ALSO

EdVest College Savings Plan L<https://www.edvest.com>

=head1 AUTHOR

Jeremy Volkening, C<< <jdv@base2bio.com> >>

=head1 CAVEATS AND BUGS

Please report any bugs or feature requests to the issue tracker
at L<https://github.com/jvolkening/p5-Finance-Quote-EdVest>.

=head1 COPYRIGHT AND LICENSE

Copyright 2018 Jeremy Volkening.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=cut

