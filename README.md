# NAME

Finance::Quote::EdVest - Obtain fund prices for EdVest plans

# SYNOPSIS

    use Finance::Quote;

    my $q    = Finance::Quote->new;
    my %info = Finance::Quote->fetch('edvest' => 'EVBOND'); 

# DESCRIPTION

This module fetches fund information for the EdVest funds

    https://www.edvest.com/

using its fund prices page

    http://www.edvest.com/research/daily.shtml

The quote symbols are

        * EVAB0    Age Band 0-4

        * EVAB5    Age Band 5-8

        * EVAB9    Age Band 9-10

        * EVAB11   Age Band 11-12

        * EVAB13   Age Band 13-14

        * EVAB15   Age Band 15

        * EVAB16   Age Band 16

        * EVAB17   Age Band 17

        * EVAB18   Age Band 18+

        * EVAGA0   Aggresive Age Band 0-4

        * EVAGA5   Aggresive Age Band 5-8

        * EVAGA9   Aggresive Age Band 9-10

        * EVAGA11  Aggresive Age Band 11-12

        * EVAGA13  Aggresive Age Band 13-14

        * EVAGA15  Aggresive Age Band 15

        * EVAGA16  Aggresive Age Band 16

        * EVAGA17  Aggresive Age Band 17

        * EVAGA18  Aggresive Age Band 18+

        * EVCD     Bank CD Portfolio

        * EVPPI    Principal Plus Interest

        * EVIBA    Index-Based Aggressive

        * EVIBM    Index-Based Moderate

        * EVIBC    Index-Based Conservative

        * EVABA   => "Active-Based Aggressive

        * EVABM   => "Active-Based Moderate

        * EVABC   => "Active-Based Conservative

        * EVI     => "International Equity Index

        * EVB     => "Balanced

        * EVSCI   => "Small-Cap Index Portfolio

        * EVUSA   => "U.S. Equity Active Portfolio

        * EVLCI   => "Large Cap Stock Index Portfolio

        * EVSOC   => "Social Choice Portfolio

        * EVBOND  => "Bond Index Portfolio
    

# SEE ALSO

EdVest College Savings Plan [https://www.edvest.com](https://www.edvest.com)
