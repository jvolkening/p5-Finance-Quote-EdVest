#!/usr/bin/env perl

use strict;
use warnings;
use 5.012;

use Test::More;
use Finance::Quote;
use Finance::Quote::EdVest;

if (not $ENV{TEST_AUTHOR}) {
    plan( skip_all => 'Author test.  Set $ENV{TEST_AUTHOR} to true to run.');
}

my $fq = Finance::Quote->new;
my $q = $fq->fetch('edvest', 'foobar');

# Sometimes Data::Dumper gets left in code by accident.  Make sure
# we haven't done so.

ok(! exists $INC{'Data/Dumper.pm'}, "Data::Dumper should not be loaded");

done_testing(1);
