#!/usr/bin/env perl

use strict;
use warnings;
use 5.012;

use Test::More;
use Finance::Quote;

# Test to see if Finance::Quote::EdVest can at least be loaded and used.
require_ok('Finance::Quote::EdVest');

done_testing(1);
