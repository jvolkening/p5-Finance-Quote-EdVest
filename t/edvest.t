#!/usr/bin/env perl

use strict;
use warnings;
use 5.012;

use Test::More;
use Finance::Quote;
use Finance::Quote::EdVest;

if (not $ENV{ONLINE_TEST}) {
    plan skip_all => 'Set $ENV{ONLINE_TEST} to run this test';
}


my %ev_funds = (
    EVEY40 => "2040/2041 Enrollment",
    EVEY38 => "2038/2039 Enrollment",
    EVEY36 => "2036/2037 Enrollment",
    EVEY34 => "2034/2035 Enrollment",
    EVEY33 => "2032/2033 Enrollment",
    EVEY30 => "2030/2031 Enrollment",
    EVEY28 => "2028/2029 Enrollment",
    EVEY26 => "2026/2027 Enrollment",
    EVEY24 => "2024/2025 Enrollment",
    EVIS   => "In School",
    EVPPI  => "Principal Plus Interest",
    EVIBA  => "Index-Based Aggressive",
    EVIBM  => "Index-Based Moderate",
    EVIBC  => "Index-Based Conservative",
    EVABA  => "Active-Based Aggressive",
    EVABM  => "Active-Based Moderate",
    EVABC  => "Active-Based Conservative",
    EVI    => "International Equity Index",
    EVB    => "Balanced",
    EVSCI  => "Small-Cap Index",
    EVUSA  => "U.S. Equity Active",
    EVLCI  => "Large-Cap Stock Index",
    EVSOC  => "Social Choice",
    EVBOND => "Bond Index",
);

# Test TSP functions.

my $quoter = Finance::Quote->new();

my $n_tests = 1;

my $quote;
ok( $quote = $quoter->edvest(keys %ev_funds), "Init quote" );

for my $fund (keys %ev_funds) {

    ok( $quote->{$fund,'success'},
        "Fetched $ev_funds{$fund}" );

    ok( $quote->{$fund, 'price'} =~ /^[\d\.]+$/,
        "Found price for $ev_funds{$fund}" );

    for (qw/
        method
        currency
        source
        symbol
        name
        nav
        last
        price
        date
        isodate
    /) {
        ok( defined $quote->{$fund, $_},
            "Found $_ for $ev_funds{$fund}" );
    }

    ok( ! defined  $quote->{$fund, 'foobar'},
        "Bogus label not found for $ev_funds{$fund}" );

    ok( ! defined  $quote->{'foobar', 'price'},
        "Bogus fund not found" );

}

$n_tests += 14 * (scalar keys %ev_funds);

# test fetch method
$quote = $quoter->fetch('edvest', keys %ev_funds);

for my $fund (keys %ev_funds) {

    ok( $quote->{$fund,'success'},
        "Fetched $ev_funds{$fund}" );

    ok( $quote->{$fund, 'price'} =~ /^[\d\.]+$/,
        "Found price for $ev_funds{$fund}" );

    for (qw/
        method
        currency
        source
        symbol
        name
        nav
        last
        price
        date
        isodate
    /) {
        ok( defined $quote->{$fund, $_},
            "Found $_ for $ev_funds{$fund}" );
    }

    ok( ! defined  $quote->{$fund, 'foobar'},
        "Bogus label not found for $ev_funds{$fund}" );

    ok( ! defined  $quote->{'foobar', 'price'},
        "Bogus fund not found" );

}

$n_tests += 14 * (scalar keys %ev_funds);

done_testing($n_tests);

